class J2paramsController < ApplicationController
	def index
		id = params[:id]
		magi = Magi.find_by id: id

		if magi 
			@magis = magi.chara
		else 
			@magis = "tidak ditemukan"
		end
	end
end