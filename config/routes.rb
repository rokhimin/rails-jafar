Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/', to: 'home#index'
  get '/helloworld', to: 'j0helloworld#index'
  get '/passingdata', to: 'j1passingdata#index'
  get '/params', to: 'j2params#index'
  get '/params/:id', to: 'j2params#index'

  resources :magi
end
